package nz.edwardhuang.smsgateway.services

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.Service
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.IBinder
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import com.github.kittinunf.result.Result
import nz.edwardhuang.smsgateway.R
import nz.edwardhuang.smsgateway.db.DAO
import nz.edwardhuang.smsgateway.extensions.TAG
import nz.edwardhuang.smsgateway.models.SMSMessage
import nz.edwardhuang.smsgateway.utils.API
import nz.edwardhuang.smsgateway.utils.SMSBroadcastReceiver
import java.util.concurrent.Executors
import java.util.concurrent.ScheduledFuture
import java.util.concurrent.TimeUnit

class SMSForegroundService : Service() {

    companion object {
        private val executor = Executors.newScheduledThreadPool(1)
        /**
         * Update the authority remotes
         */
        val getAuthorityRunnable: () -> Runnable = {
            object : Runnable {
                override fun run() {
                    when (val r = API.getRemoteURLsFromAuthority().third) {
                        is Result.Success -> {
                            Log.d(TAG, "Getting authority success")
                            DAO.setRemoteURLs(r.get().remoteURLs)
                        }
                        is Result.Failure -> {
                            Log.d(TAG, "Getting authority failed: ${r.getException()}")
                        }
                    }
                }
            }
        }
    }

    private var listener: SMSBroadcastReceiver.SMSListener? = null
    private var task: ScheduledFuture<*>? = null

    override fun onBind(intent: Intent?): IBinder? {
        return null // no bind1
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun createNotificationChannel(channelId: String, channelName: String): String {
        val chan = NotificationChannel(
            channelId,
            channelName, NotificationManager.IMPORTANCE_NONE
        )
        chan.lightColor = Color.BLUE
        chan.lockscreenVisibility = Notification.VISIBILITY_PRIVATE
        val service = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        service.createNotificationChannel(chan)
        return channelId
    }

    override fun onCreate() {
        super.onCreate()
        Log.d(TAG, "Service create")
        task = executor.scheduleAtFixedRate(getAuthorityRunnable(), 0, 100, TimeUnit.SECONDS)
        listener = SMSBroadcastReceiver.addListener(object : SMSBroadcastReceiver.SMSListener {
            override fun onSMSReceive(msg: SMSMessage) {
            }
        })
    }

    override fun onDestroy() {
        Log.d(TAG, "Service destroy")
        if (listener != null) {
            SMSBroadcastReceiver.removeListener(listener!!)
        }
        task?.cancel(true)
        task = null
        listener = null
        startService(Intent(this, SMSForegroundService::class.java))
        super.onDestroy()
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        val channelId =
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                createNotificationChannel("my_service", "My Background Service")
            } else {
                // If earlier version channel ID is not used
                // https://developer.android.com/reference/android/support/v4/app/NotificationCompat.Builder.html#NotificationCompat.Builder(android.content.Context)
                ""
            }

        val notification: Notification = NotificationCompat.Builder(this, channelId)
            .setContentTitle(getText(R.string.notification_title))
            .setContentText(getText(R.string.notification_message))
            .setSmallIcon(R.drawable.ic)
            .setPriority(0)
            .build();

        notification.flags = notification.flags.or(Notification.FLAG_ONGOING_EVENT);

        Log.d(TAG, "Starting foreground service...")
        startForeground(1, notification)
        return START_STICKY
    }

}