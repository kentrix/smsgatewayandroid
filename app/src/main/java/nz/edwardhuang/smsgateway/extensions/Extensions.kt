package nz.edwardhuang.smsgateway.extensions

val Any.TAG : String
  get() = this.javaClass.canonicalName.toString()