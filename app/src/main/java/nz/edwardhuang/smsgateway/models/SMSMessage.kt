package nz.edwardhuang.smsgateway.models

import nz.edwardhuang.smsgateway.BuildConfig
import java.util.*

data class SMSMessage(val from: String, val body: String, val receivedAt: Date, val phoneNumber: String): Convertable<SMSMessageWithAPIKey> {
    override fun convert(): SMSMessageWithAPIKey {
        return SMSMessageWithAPIKey(from, body, receivedAt, phoneNumber, BuildConfig.API_KEY)
    }

}

data class SMSMessageWithAPIKey(val from: String, val body: String, val receivedAt: Date, val phoneNumber: String, val apiKey: String): Convertable<SMSMessage> {
    override fun convert(): SMSMessage {
        return SMSMessage(from, body, receivedAt, phoneNumber)
    }
}

interface Convertable<TARGET> {
    fun convert(): TARGET
}

