package nz.edwardhuang.smsgateway.models

import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.Gson

data class AuthorityResponse(val remoteURLs: List<String>) {
    class AuthorityResponseDeseriliser : ResponseDeserializable<AuthorityResponse> {
        override fun deserialize(content: String): AuthorityResponse? {
            return Gson().fromJson(content, AuthorityResponse::class.java)
        }
    }
}