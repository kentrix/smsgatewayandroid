package nz.edwardhuang.smsgateway.activities

import android.Manifest
import android.annotation.SuppressLint
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.telephony.TelephonyManager
import android.text.Editable
import android.util.AttributeSet
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationCompat.PRIORITY_MIN
import androidx.core.content.ContextCompat
import nz.edwardhuang.smsgateway.R
import nz.edwardhuang.smsgateway.db.DAO
import nz.edwardhuang.smsgateway.extensions.TAG
import nz.edwardhuang.smsgateway.services.SMSForegroundService
import java.util.concurrent.atomic.AtomicInteger

class MainActivity : AppCompatActivity() {

    private lateinit var phoneNumberField: EditText
    private lateinit var saveBtn: Button

    @SuppressLint("MissingPermission")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        checkForPermissions()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            startForegroundService(Intent (this, SMSForegroundService::class.java));
        } else {
            startService(Intent(this, SMSForegroundService::class.java))
        }

        phoneNumberField = findViewById(R.id.phoneNumberField)

        phoneNumberField.setText(DAO.getPhoneNumber(), TextView.BufferType.EDITABLE)

        saveBtn = findViewById(R.id.saveButton)

        saveBtn.setOnClickListener {
            DAO.setPhoneNumber(phoneNumberField.text.toString())
            Toast.makeText(this, "Saved!", Toast.LENGTH_LONG).show()
        }
    }

    private val requestCode = AtomicInteger(1)

    private fun checkForPermissions() {
        val toCheck = listOf(
            Manifest.permission.READ_SMS,
            Manifest.permission.RECEIVE_SMS,
            Manifest.permission.SEND_SMS,
            Manifest.permission.FOREGROUND_SERVICE,
            Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.WAKE_LOCK,
            Manifest.permission.RECEIVE_BOOT_COMPLETED
        )
        Log.d(TAG, "Checking for permission $toCheck")
        toCheck.forEach {
            Log.d(TAG, "Checking for permission $it")
            if (ContextCompat.checkSelfPermission(this, it) != PackageManager.PERMISSION_GRANTED) {
                Log.d(TAG, "Asking for permission $it")
                ActivityCompat.requestPermissions(this, arrayOf(it), requestCode.getAndIncrement())
            } else {
                Log.d(TAG, "Permission already granted for $it")
            }
            return@forEach
        }
    }

}
