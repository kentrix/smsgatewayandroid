package nz.edwardhuang.smsgateway.db

import android.util.Log
import io.paperdb.Paper
import nz.edwardhuang.smsgateway.extensions.TAG
import nz.edwardhuang.smsgateway.models.SMSMessage
import java.util.*
import kotlin.collections.ArrayList

object DAO {
    private const val SMSMessagesKey = "SMSMessages"
    private const val UUIDKey = "UUID"
    private const val RemoteURLsKey = "RemoteURLs"
    private const val PhoneNumberKey = "PhoneNumber"

    fun getAllPendingSMS(): ArrayList<SMSMessage> {
        Log.d(TAG, "Getting all pending messages")
        return Paper.book().read(SMSMessagesKey)
    }

    fun getAPendingSMS(): SMSMessage? {
        val list = getAllPendingSMS()
        Log.d(TAG, "Getting first pending message, list size: ${list.size}")
        return list.firstOrNull()
    }

    fun saveAPendingSMS(msg: SMSMessage) {
        Log.d(TAG, "Saving a message: $msg")
        val list = getAllPendingSMS()
        list.add(msg)
        Paper.book().write(SMSMessagesKey, list)
    }

    fun getUUID(): UUID {
        Log.d(TAG, "Getting UUID")
        val uuid = Paper.book().read<UUID>(UUIDKey)
        return if (uuid == null) {
            Log.d(TAG, "No UUID, generating one")
            val gen = UUID.randomUUID()
            Paper.book().write(UUIDKey, gen)
            gen
        } else {
            uuid
        }
    }

    fun getRemoteURLs(): List<String> {
        Log.d(TAG, "Getting remote URLs")
        return Paper.book().read<List<String>>(RemoteURLsKey, listOf())
    }

    fun setRemoteURLs(l: List<String>) {
        Log.d(TAG, "Setting remote URLs: $l")
        Paper.book().write(RemoteURLsKey, l)
    }

    fun getPhoneNumber(): String {
        Log.d(TAG, "Getting phone number")
        return Paper.book().read<String>(PhoneNumberKey, "UNKNOWN")
    }

    fun setPhoneNumber(n: String) {
        Log.d(TAG, "Setting phone number")
        Paper.book().write(PhoneNumberKey, n)
    }
}