package nz.edwardhuang.smsgateway.utils

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.widget.Toast
import androidx.core.content.ContextCompat
import nz.edwardhuang.smsgateway.extensions.TAG
import nz.edwardhuang.smsgateway.services.SMSForegroundService

class BootupReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        Toast.makeText(context, "OnReceive $TAG",Toast.LENGTH_LONG).show()
        val am = context?.getSystemService(Context.ALARM_SERVICE) as AlarmManager

        val pi = PendingIntent.getService(context, 0, Intent(context, SMSForegroundService::class.java), PendingIntent.FLAG_ONE_SHOT)

        ContextCompat.startForegroundService(context, Intent(context, SMSForegroundService::class.java))

        am.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), 5000, pi)
    }
}