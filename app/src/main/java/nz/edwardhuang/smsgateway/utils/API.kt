package nz.edwardhuang.smsgateway.utils

import android.util.Log
import com.github.kittinunf.fuel.core.ResponseResultOf
import com.github.kittinunf.fuel.gson.jsonBody
import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.fuel.httpPost
import com.github.kittinunf.result.Result
import nz.edwardhuang.smsgateway.BuildConfig
import nz.edwardhuang.smsgateway.db.DAO
import nz.edwardhuang.smsgateway.extensions.TAG
import nz.edwardhuang.smsgateway.models.AuthorityResponse
import nz.edwardhuang.smsgateway.models.SMSMessage
import java.util.concurrent.Executors

object API {
    fun postASMSMessage(msg: SMSMessage) {
        val rs = DAO.getRemoteURLs()
        val exe = Executors.newFixedThreadPool(rs.size)
        DAO.getRemoteURLs().forEach {
            exe.submit {
                it
                    .httpPost()
                    .jsonBody(msg.convert())
                    .also {
                        Log.d(TAG, "$it")
                    }
                    .response { _, _, result ->
                        when (result) {
                            is Result.Success -> {
                                Log.d(TAG, "Post successful, ${result.get()}")
                            }
                            is Result.Failure -> {
                                Log.e(TAG, "Post failed, ${result.getException()}")
                            }
                        }
                    }
            }
        }
    }

    fun getRemoteURLsFromAuthority(): ResponseResultOf<AuthorityResponse> {
        return "${BuildConfig.AUTHORITY_URL}".httpGet().responseObject(AuthorityResponse.AuthorityResponseDeseriliser())
    }

}