package nz.edwardhuang.smsgateway.utils

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Build
import android.provider.Telephony
import android.util.Log
import android.widget.Toast
import nz.edwardhuang.smsgateway.db.DAO
import nz.edwardhuang.smsgateway.extensions.TAG
import nz.edwardhuang.smsgateway.models.SMSMessage
import java.util.*
import kotlin.collections.HashSet

class SMSBroadcastReceiver : BroadcastReceiver() {
    companion object {
        private val listeners: MutableSet<SMSListener> = HashSet()

        private val defaultListener = object: SMSListener{
            override fun onSMSReceive(msg: SMSMessage) {
                API.postASMSMessage(msg)
            }
        }

        fun addListener(l: SMSListener): SMSListener {
            listeners.add(l)
            return l
        }

        fun removeListener(l: SMSListener) {
            listeners.remove(l)
        }

        fun clearListeners() {
            listeners.clear()
        }
    }

    override fun onReceive(context: Context?, intent: Intent) {
        Toast.makeText(context, "onReceiveSMS Called", Toast.LENGTH_LONG).show()
        if (intent.action.equals(Telephony.Sms.Intents.SMS_RECEIVED_ACTION)) {
            var sender: String = ""
            var body: String = ""

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                Telephony.Sms.Intents.getMessagesFromIntent(intent).forEach {
                    sender = it.displayOriginatingAddress
                    body += it.displayMessageBody
                }
                val msg = SMSMessage(sender, body, Date(), DAO.getPhoneNumber())
                Log.d(TAG, "Received Message $msg")
                listeners.forEach {
                    it.onSMSReceive(msg)
                }
                defaultListener.onSMSReceive(msg)
            } else {
                Log.e(TAG, "Android lower than 4.4 is not supported atm!")
            }
        }
    }

    interface SMSListener {
        fun onSMSReceive(msg: SMSMessage)
    }

}