package nz.edwardhuang.smsgateway.application

import android.app.Application
import android.content.Context
import android.content.IntentFilter
import android.os.Build
import android.provider.Telephony
import android.telephony.TelephonyManager
import android.util.Log
import androidx.annotation.RequiresApi
import io.paperdb.Paper
import nz.edwardhuang.smsgateway.extensions.TAG
import nz.edwardhuang.smsgateway.utils.SMSBroadcastReceiver


class SMSGateway : Application() {
    @RequiresApi(Build.VERSION_CODES.KITKAT)
    override fun onCreate() {
        super.onCreate()
        Log.d(TAG, "Application init")
        // val receiver = SMSBroadcastReceiver()
        // registerReceiver(receiver, IntentFilter(Telephony.Sms.Intents.SMS_RECEIVED_ACTION))
        Paper.init(this)
        instance = this;
    }


    companion object {
        private lateinit var instance: SMSGateway
    }
}